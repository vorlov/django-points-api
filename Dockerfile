FROM python:3.8

RUN pip install poetry \
    && apt-get update \
    && apt-get install -y netcat

WORKDIR /app

ADD poetry.lock pyproject.toml /app/
ADD . /app

RUN poetry config virtualenvs.create false \
    && poetry install --no-interaction --no-ansi \
    && poetry add gunicorn \
    && chmod +x deploy/wait-for \
    && chmod a+x deploy/docker-entrypoint.sh

ENTRYPOINT ["deploy/docker-entrypoint.sh"]

# Django points system API

This is project created for test. The api provides user points control for business companies.

Created with python and django rest framework.

## Install

    git clone ... .
    pip install poetry
    poetry install

Create `.env` file and fill it with `.env.dist` example

### Environment file description:

#### Django

* `DJ_SECRET_KEY` - django production secret key. Generate your own.

#### Postgres

* `PG_DB_NAME` - database name.
* `PG_USER` - username.
* `PG_PASSWORD` - user password.
* `PG_HOST` - database host.
* `PG_PORT` - database port (default: 5432).

#### Redis

* `REDIS_DB` - database number (default: 0).
* `REDIS_HOST` - database host.
* `REDIS_PORT` - database port (default: 6379).
* `REDIS_PASSWORD` - database password (if no password, then leave blank)

#### Clickhouse

* `CH_DB_URL` - database url.
* `CH_DB_NAME` - database name.
* `CH_USERNAME` - username.
* `CH_PASSWORD` - user password (default: blank)

## Run the app

    gunicorm main.wsgi

## REST API

The REST API to the point app is described below. Available only if api key in `X-API-KEY` header

### Get user info

#### Request:

* Method: `GET`
* Path: `/api/v1/user`

#### Success response:

```
{
    "status": "success",
    "message": "",
    "data": {
        "username": "robertrobinson",
        "first_name": "Jill",
        "last_name": "Cline",
        "api_key": "Kdz_mPMEYZZcT4q3lfVCAPYVsZc288tu8eB-lhPNseQ",
        "balance": 74845
    }
}
```

#### Error response:

```
{
    "status": "error",
    "message": "API key is not provided or invalid. Check \"X-API-KEY\" header",
    "data": {}
}
```

### Get list of transactions

#### Request:

* Method: `GET`
* Path: `/api/v1/transaction`

#### Success response:

```
{
    "status": "success",
    "message": "",
    "data": [
        {
            "sum": -2499,
            "description": "Similar treat various might law. Significant yet knowledge opportunity."
        },
        {
            "sum": 1000000,
            "description": ""
        }
    ]
}
```

### Create new transaction

#### Request:

* Method: `POST`
* Path: `/api/v1/transaction`
* Post data:

```
{
    "sum": "uint",
    "description": "string"
}
```

#### Success response:

```
{
    "status": "success",
    "message": "",
    "data": {
        "sum": 212,
        "description": "For new laptop"
    }
}
```

#### Error response:

```
{
    "status": "error",
    "message": "Not enough points",
    "data": {}
}
```
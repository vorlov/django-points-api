#!/bin/bash -x

./deploy/wait-for ${PG_HOST}:${PG_PORT:-5432} -- \
./deploy/wait-for ${REDIS_HOST}:${REDIS_PORT:-6379} -- \
./deploy/wait-for ${CH_DB_URL} -- \
python manage.py migrate --noinput || exit 1

exec "$@"
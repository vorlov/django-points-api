from rest_framework import serializers
from .models import Transaction, User


# This serializer works only if `many=True` is set in Serializer initial argument
class CustomListSerializer(serializers.ListSerializer):
    def is_valid(self, *args, **kwargs):
        if not self.context.get('user'):
            raise AssertionError('"user" key is required in Serializer `context` property')
        return super().is_valid(*args, **kwargs)


class TransactionSerializer(serializers.Serializer):
    # Setting `read_only=True` we want that fields won't be changed
    sum = serializers.IntegerField()
    description = serializers.CharField(required=False, max_length=2000)

    def create(self, validated_data):
        return Transaction.objects.create(
            # This key is free to set, because method `is_valid()` has overridden down bellow
            user=self.context['user'],
            **validated_data
        )

    # Additional secure to prevent "user" key missing
    def is_valid(self, *args, **kwargs):
        if not self.context.get('user'):
            raise AssertionError('"user" key is required in Serializer `context` property')
        return super().is_valid(*args, **kwargs)

    # This method override allows to return custom list serializer.
    # In this case we use it for `is_valid()` method override
    @classmethod
    def many_init(cls, *args, **kwargs):
        kwargs['child'] = cls()
        return CustomListSerializer(*args, **kwargs)


class UserSerializer(serializers.Serializer):
    username = serializers.CharField(read_only=True)
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    api_key = serializers.CharField(read_only=True)
    balance = serializers.IntegerField(min_value=0)

from celery import shared_task
from celery.utils.log import get_task_logger
from .models import User
from .clickhouse_models import ClickHouseBalance
from datetime import datetime

logger = get_task_logger(__name__)


@shared_task
def pull_balance_to_clickhouse():
    users = User.objects.all()

    total_balance = 0
    for user in users:
        total_balance += user.balance

    ClickHouseBalance.objects.create(balance=total_balance, created=datetime.now())

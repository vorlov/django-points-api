from rest_framework.views import APIView
from .types import MutatedRequest
from utils.response import ResponseGenerator

from .models import Transaction
from .serializers import TransactionSerializer, UserSerializer


class TransactionView(APIView):
    # Get all transactions
    def get(self, request: MutatedRequest):
        transactions = Transaction.objects.filter(user=request.user_)
        serializer = TransactionSerializer(transactions, many=True, context={'user': request.user_})

        return ResponseGenerator.json(
            status_code=200,
            status='success',
            data=serializer.data
        )

    # This `post` request creates new transaction.
    # After we change balance of User
    def post(self, request: MutatedRequest):
        serializer = TransactionSerializer(data=request.data, context={'user': request.user_})
        if serializer.is_valid():
            # Save transaction
            serializer.save()
            # Change and save user balance
            # Do not use `data` because it's available only once. Use `validated_data`
            new_balance = request.user_.balance + int(serializer.validated_data['sum'])

            if new_balance < 0:
                return ResponseGenerator.json(
                    status_code=400,
                    status='error',
                    message='Not enough points'
                )

            request.user_.balance = new_balance
            request.user_.save()

            return ResponseGenerator.json(
                status_code=200,
                status='success',
                data=serializer.data
            )
        else:
            return ResponseGenerator.json(
                status_code=400,
                status='error',
                message='',
                data=serializer.errors
            )


class UserView(APIView):
    def get(self, request: MutatedRequest):
        serializer = UserSerializer(request.user_)
        return ResponseGenerator.json(
            status_code=200,
            status='success',
            data=serializer.data
        )

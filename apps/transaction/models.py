from django.db import models
import secrets


# Create your models here.
class _TimeStamped(models.Model):
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:
        abstract = True


class User(_TimeStamped):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Set previous fields value in class attribute
        self.__prev_fields = ['balance']
        for _prev_field in self.__prev_fields:
            setattr(self, f'__prev_{_prev_field}', getattr(self, _prev_field))

    username = models.TextField(null=False)
    first_name = models.TextField(null=False)
    last_name = models.TextField(null=False)
    api_key = models.TextField(default=secrets.token_urlsafe)
    balance = models.PositiveIntegerField(default=0)

    class Meta:
        db_table = 'user'

    def save(self, *args, **kwargs):
        # Get previous value
        prev_balance = getattr(self, '__prev_balance')
        if prev_balance < 1000:
            if self.balance >= 1000:
                print('Grats, you reached 1000 points')

        if prev_balance < 100000:
            if self.balance >= 100000:
                print('Grats, you reached 100000 points')

        if self.balance == 0:
            print('You have 0 points :(')

        print(prev_balance, self.balance)
        super().save(*args, **kwargs)


class Transaction(_TimeStamped):
    description = models.TextField()
    sum = models.IntegerField(null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'transaction'

from django_clickhouse import migrations
from apps.transaction.clickhouse_models import ClickHouseBalance


class Migration(migrations.Migration):
    operations = [
        migrations.CreateTable(ClickHouseBalance)
    ]

from .models import User
from .types import MutatedRequest
from utils.response import ResponseGenerator

from rest_framework.request import Request


class FieldsLoaderMiddleware:
    """
    This middleware loads fields to request to prevent double pull from database.
    Must be the first on order!
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request: Request):
        setattr(request, 'user_', self._return_user(request))

        response = self.get_response(request)
        return response

    def _return_user(self, request: Request):
        api_key = request.META.get('HTTP_X_API_KEY') or None
        user = User.objects.filter(api_key=api_key).first()
        return user


class FieldsCheckerMiddleware:
    """
    This middleware checks loaded fields from `FieldsLoaderMiddleware`,
    so we prevents all runtime type errors related to this properties.
    If any condition is False, than service throws 403
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request: MutatedRequest):
        if not request.user_ and not isinstance(request.user_, User):
            return ResponseGenerator.json(
                status_code=403,
                status='error',
                message='API key is not provided or invalid. Check "X-API-KEY" header',
            )

        response = self.get_response(request)
        return response

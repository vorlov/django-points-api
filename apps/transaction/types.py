from rest_framework.request import Request

from .models import User


# Interface to additional request fields
class MutatedRequest(Request):
    user_: User

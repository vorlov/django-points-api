from django.urls import path
from .views import TransactionView, UserView

urlpatterns = [
    path('transaction', TransactionView.as_view()),
    path('user', UserView.as_view())
]

from django_clickhouse.clickhouse_models import ClickHouseModel
from django_clickhouse.engines import MergeTree
from infi.clickhouse_orm import fields


class ClickHouseBalance(ClickHouseModel):
    balance = fields.Int64Field()
    created = fields.DateField()

    engine = MergeTree('created', ('balance',))

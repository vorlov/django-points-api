import os
from pathlib import Path
from dotenv import load_dotenv

BASE_DIR = Path(__file__).resolve().parent.parent
LOG_DIR = BASE_DIR / 'logs'
LOG_DIR.mkdir(exist_ok=True)

MUST_HAVE_ENV = [
    'DJ_SECRET_KEY',

    'PG_DB_NAME',
    'PG_USER',
    'PG_PASSWORD',
    'PG_HOST',

    'REDIS_DB',
    'REDIS_HOST',
    'REDIS_PORT',
    'REDIS_PASSWORD',

    'CH_DB_URL',
    'CH_DB_NAME',
    'CH_USERNAME'
]

load_dotenv(BASE_DIR / '.env')

for env_key in MUST_HAVE_ENV:
    if env_key not in os.environ:
        raise EnvironmentError(f'{env_key} must be in env variables')

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('DJ_SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.getenv('DJ_DEBUG', True)

ALLOWED_HOSTS = ['*']

# Application definition
INSTALLED_APPS = [
    'django_seed',
    'django_clickhouse',
    'rest_framework',
    'apps.transaction'

]

MIDDLEWARE = [
    # Django docs recommends use at least CommonMiddleware
    # https://docs.djangoproject.com/en/3.2/topics/http/middleware/
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'apps.transaction.middlewares.FieldsLoaderMiddleware',
    'apps.transaction.middlewares.FieldsCheckerMiddleware'
]

# Toggles off user and auth (in this project we use custom auth)
# Source: https://stackoverflow.com/questions/27085219/how-can-i-disable-authentication-in-django-rest-framework
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [],
    'DEFAULT_PERMISSION_CLASSES': [],
    'UNAUTHENTICATED_USER': None,
}

ROOT_URLCONF = 'main.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'main.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('PG_DB_NAME'),
        'USER': os.getenv('PG_USER'),
        'PASSWORD': os.getenv('PG_PASSWORD'),
        'HOST': os.getenv('PG_HOST'),
        'PORT': os.getenv('PG_PORT', 5432)
    }
}

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/


# Celery configuration
CELERY_BROKER_URL = f'redis://{os.getenv("REDIS_HOST")}:{os.getenv("REDIS_PORT", 6379)}'
CELERY_RESULT_BACKEND = CELERY_BROKER_URL

CELERY_BEAT_SCHEDULE = {
    'pull_balance_to_clickhouse': {
        'task': 'apps.transaction.tasks.pull_balance_to_clickhouse',
        'schedule': 60
    }
}

# Clickhouse configuration
CLICKHOUSE_DATABASES = {
    'default': {
        'db_url': os.getenv('CH_DB_URL'),
        'db_name': os.getenv('CH_DB_NAME'),
        'username': os.getenv('CH_USERNAME'),
        'password': os.getenv('CH_PASSWORD', '')
    }
}

# Logging configuration
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'celery': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'logs/celery.log',
            'maxBytes': 1024 * 1024 * 10,
            'backupCount': 5,
            'formatter': 'standard'
        },
        'console': {
            'level': 'WARNING',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'django_request': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'logs/django_request.log',
            'maxBytes': 1024 * 1024 * 10,
            'backupCount': 5,
            'formatter': 'standard'
        },
        'django': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'logs/django.log',
            'maxBytes': 1024 * 1024 * 10,
            'backupCount': 5,
            'formatter': 'standard'
        }
    },
    'loggers': {
        'django': {
            'handlers': ['django'],
            'level': 'INFO',
            'propagate': True
        },
        'django_console': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': True
        },
        'django.request': {
            'handlers': ['django_request'],
            'level': 'INFO',
            'propagate': True,
        },

        'celery': {
            'handlers': ['celery'],
            'level': 'INFO',
            'propagate': True
        }
    }
}

# Others
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

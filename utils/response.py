import typing
from django.http import JsonResponse as _JsonResponse


class ResponseGenerator:
    @classmethod
    def json(cls, status_code: int,
             status: typing.Literal['success', 'error'],
             message: typing.Optional[str] = '',
             data: typing.Optional[typing.Dict] = None) -> _JsonResponse:
        return _JsonResponse(
            status=status_code,
            data={
                'status': status,
                'message': message,
                'data': data if data else {}
            }
        )
